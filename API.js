/* API 規格書 使用 node.js */
// 以功能區分API 
const API = {
  member:{
    //會員登入 with email / password
    login: {
      url: "/v1/accounts:signInWithPassword",
      method: "post"
    },
    //會員註冊 with email / password
    registered: {
      url: "/v1/accounts:signUp",
      method: "post"
    },
    //使用 refresh token 換取 ID token
    exchangeToken: {
      url: "/v1/token",
      method: "post"
    },
  },
  //取得課程資訊
  getCourses: {
    url: "/courses/:id.json",
    method: "get"
  },
  //取得課程資訊 - 首頁列表
  getCoursesList: {
    url: "/courses.json",
    method: "get"
  },
  //取得課程內容
  getCoursesItem: {
    url: "/courses_item/:id.json",
    method: "get"
  },
};
// node.js 寫法
module.exports = API;