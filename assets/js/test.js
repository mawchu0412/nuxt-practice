function test1(name) {
    return name;
 }
 function test2(name) {
    return name;
 }
//  方法1 全部匯出 倒出物件使用 dot notation 調用
 export default {
    test1,
    test2
 };
//方法2 局部匯出
//  export default test1;