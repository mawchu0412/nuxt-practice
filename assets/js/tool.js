export const sleep = millisecond => {
    return new Promise(resolve => {
        setTimeout(resolve, millisecond);
    });
}

export const deepCopy = object => {
	let info = "";
	!!object && (info = JSON.parse(JSON.stringify(object)));
	return info;
};