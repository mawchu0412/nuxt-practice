export default function ({ store, redirect, req }) {
  // 由於 middleware 鉤子前後端都會跑 需要兩端都處理：
  //server side
  if(process.server) {
    // 沒有設定 cookie 或沒有 token 先導回首頁
    if(!req.headers.cookie) return redirect('/');
    if(req.headers.cookie.indexOf('id_token') == -1) return redirect('/');
  }
  //client side
  // If the user is not authenticated
  if (!store.state.isUserLoggedIn) {
    return redirect('/')
  }
}