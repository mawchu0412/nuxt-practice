// by module
const sleep = require('./utility/sleep') 
// by export
// const {sleep} = require('./utility/sleep') 

// 使用原生 node
// const http = require('http');
// http.createServer(async(req, res) => {
//     await sleep(5000);
//     console.log(process.env.PORT)
//     res.end('Hello world 123')
// }).listen(process.env.PORT || 3000)

// 使用 express 框架
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const querystring = require('querystring');

// cors
const cors = require('./utility/cors') 
// 在每個路由訪問之前添加擋頭
app.use('/*', cors); 

const test = require('./routers/test');
app.use('/api', test);

const wayne1894_nuxt = require("./routers/wayne1984_nuxt");
app.use('/', wayne1894_nuxt);


// app.get('/', (req, res) => {
//     res.send('hello world123')
// });
app.listen(port, ()=>{
    console.log(`mock server listening at http://localhost:${port}`)
})