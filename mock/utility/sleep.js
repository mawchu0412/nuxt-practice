const sleep = ms => {
    return new Promise(resolve => {
         setTimeout(resolve, ms);
    });
}

module.exports = sleep // by module
//exports.sleep = sleep; // by export