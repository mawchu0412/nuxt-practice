import {sleep,deepCopy} from "./assets/js/tool.js"
// 自定義路由
import router from "./router.js"
import webpack from 'webpack'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  // mode: 'spa',
  debug: true,
  head: {
    title: process.env.npm_package_name || '', // 抓 package.json 檔案的 name
    // title: '我想要的網站名稱',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel:"stylesheet", type:"text/css", href:'https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css' }
    ],
    script:
    [
      { src: "/jquery.min.js"},
      { src: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.js" },
      {}
    ]

  },
  loading: {
    color: 'DodgerBlue',
    height: '10px',
    continuous: true,
    duration: 3000
  },
  layoutTransition: {
    name: 'page',
    mode: 'out-in'
  },
  pageTransition: {
    name: 'page',
    mode: 'out-in'
  },
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    // '~/assets/scss/demo.scss'
    '~assets/scss/main.scss'
  ],
  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {
      src: '~/plugins/datepicker.js',
      mode: 'client'
    },
    {
      src: '~/plugins/gsap.js',
      mode: 'client'
    },
    {
      src: '~/plugins/gtag.js',
      mode: 'client'
    },
    {
      src: '~/plugins/qs.js'
    },
    {
      src: '~/plugins/axios.js'
    }
  ],
  googleAnalytics: {
    id: 'G-2HMJX00D2P'
  },
  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,
  
  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/google-analytics',
    //['@nuxtjs/dotenv'], //這是給預設 .env 使用 nuxt 2.13版以上這行要刪除 不然會以空值覆蓋掉下面的設定。
    ['@nuxtjs/dotenv', { filename: '.env.'+ process.env.NODE_ENV }]
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    // '@nuxtjs/proxy'
  ],
  // proxy: ['http://localhost:3034/api'],
  // proxy: {
  //   '/api': 'http://localhost:3034',
  // },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    /* webpack設定放這 */
    transpile: [
      'lodash-es'
    ],
    plugins: [
      new webpack.ProvidePlugin({
        // global modules
        _C: '~/utility/constants.js',
        _: 'lodash-es'
      })
    ],
    /* 調整圖片壓縮BASE64上限*/
    // loaders: {
    //   imgUrl: { limit: 1000 },
    // },
    /* 客製化 WEBPACK*/
    // extend(config, { isClient }) {
    //   // Extend only webpack config for client-bundle
    //   if (isClient) {
    //     console.log("config:", config)
    //     config.devtool = 'source-map'
    //   }
    // }
    extractCSS: true
  },
  // router: {
  //   // mode: 'hash', //這段可以不用寫
  //   extendRoutes (routes, resolve) {
  //     return router
  //   }
  // }

  // for api
  serverMiddleware: [
    { path: '/api', handler: '~/serverMiddleware/api.js' },
    { path: '/auth', handler: '~/serverMiddleware/auth.js' },
  ],
  // env:  {
  //   apikey: 'AIzaSyDqZhxD5hACvgjcNO3ktrxHc2oJAkxC6FE'
  // }

}
