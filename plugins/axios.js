// 每次執行 axios 都會先跑這邊
export default function ({ $axios, redirect }) {

  //$axios.setHeader('Authorization', '123');// 也可以用這種方式在 header 設定 token
  //預設的 api 路徑
  $axios.setBaseURL(process.env.api_url)

  $axios.onRequest(config => {
    // console.log(config)
    // console.log('Making request to ' + config.url)
    // 每次請求傳送 firebase 的 key
    config.params = {
      key: process.env.firebaseApiKey
    }
  })

  $axios.onError(error => {
    const code = parseInt(error.response && error.response.status)
    if (code === 400) {
      redirect('/400')
    }
  })
}