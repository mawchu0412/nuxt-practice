import Vue from 'vue';
import gsap from 'gsap';
// 用全域的方式註冊 但不汙染全域環境 各自切割自己的scope(作用域)
Vue.prototype.$gsap = gsap;