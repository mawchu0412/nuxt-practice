// import Vue from 'vue'
// import VueGtag from 'vue-gtag'

// Vue.use(VueGtag, {
//   config: { id: 'G-S7EEPGECQH' }
// })

// import Vue from 'vue';
// import VueGtag from 'vue-gtag';

// if (process.env.NODE_ENV === "production") {
//     Vue.use(VueGtag, {
//         config: {id: 'G-S7EEPGECQH'},
//         appName: 'mawchu-nuxt',
//     });
// }

import Vue from 'vue'
import VueGtag from 'vue-gtag'

Vue.use(VueGtag, {
  config: { id: 'G-2HMJX00D2P' }  // 这里修改为你的gtag id，应该是G开头的。
});

// export default ({ app }) => {
//     Vue.use(VueGtag, {
//       config: { id: 'G-2HMJX00D2P' },
//       appName: 'app-name',
//     }, app.router);
//   }