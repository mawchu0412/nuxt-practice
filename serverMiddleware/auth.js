const express = require('express');
const app = express();
const axios = require('axios');
const qs = require('querystring');
let env = {
    ...require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` }).parsed
};
console.log('env', env)

// console.log('env', env.config({ path: '.env.'+ process.env.NODE_ENV }).parsed)
// env {                                                                                                                                
//     NODE_ENV: 'dev',
//     firebaseApiKey: 'AIzaSyDqZhxD5hACvgjcNO3ktrxHc2oJAkxC6FE',
//     google_api_url: 'http://localhost:3034',
//     api_url: 'http://localhost:3034'
//   }

// 第一步 透過登入Google取得Authorization code
const CLIENT_ID = '803247142182-4rg2j01mr2kgenhn8g4rlcv6lohjchnb.apps.googleusercontent.com';
const CLIENT_SECRET = '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';
const google_Auth_baseURL = 'https://accounts.google.com/o/oauth2/v2/auth';
// const process_URL = 'http://localhost:3013';
console.log(process.env.NODE_ENV)
//改成透過環境檔引入敏感資料
// const google_Auth_baseURL = env.google_Auth_baseURL;
// const process_URL = env.web_url;
const process_URL = 'https://mawchunuxt.club';
const scopes = 'openid%20email%20profile';

// if(process.env.NODE_ENV == 'prod') process_URL = 'https://mawchunuxt.club';


//先在中間程式將request header資料存在google auth state中 以利於後續跳轉回來
app.get('/', (req, res)=>{
    
    //The Referer HTTP request header contains an absolute or partial address of the page that makes the request. 
    //從哪個網頁來的
    const referer = req.headers.referer;
    if(!referer){
        res.redirect('/');
        res.end();
    }
    const authURL = `${google_Auth_baseURL}?response_type=code&client_id=${CLIENT_ID}&scope=${scopes}&redirect_uri=${process_URL}/auth/google&state=${referer}`;
    // console.log(authURL)
    res.redirect(authURL)
});

app.get('/google', async(req, res)=> {

    // 接收來源網址 Referer(存在state參數)
    let referer = req.query.state;
    // 判斷有無參數  
    if(referer.indexOf('?')> -1) {
        //有參數後面接'&'
        referer = `${referer}&`
    } else { 
        //沒參數後面接'?'
        referer = `${referer}?`
    }
    // console.log(referer)
    //取得 google 的 access_token
    const token_URL = 'https://oauth2.googleapis.com/token';
    const token_options = {
        code: req.query.code, // grant code
        client_id: CLIENT_ID,
        client_secret: CLIENT_SECRET,
        redirect_uri: `${process_URL}/auth/google`,
        grant_type: 'authorization_code'
    };
    // 文件中有要求格式為 application/x-www-form-urlencoded
    const contentType = { 
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' } 
    };
    let id_token = '';
    let access_token = '';
    // obj 轉 application/x-www-form-urlencoded 需透過 qs.stringify 套件
    await axios.post(token_URL, qs.stringify(token_options), contentType)
        .then((res)=> {
            //拿到該使用者的專屬 google access token
            //id token 塞到前端解密後可以獲得一些使用者信息
            id_token = res.data.id_token;
            access_token = res.data.access_token;
        })
        .catch((err)=>{
            console.log(err)
        });
    // console.log(qs.stringify(token_options))
    let firebase_id_token = '';
    let refresh_token = '';
    const firebase_apikey = 'AIzaSyDqZhxD5hACvgjcNO3ktrxHc2oJAkxC6FE';
    // const firebase_apikey = env.firebaseApiKey;
    const firebase_token_URL = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithIdp?key='+firebase_apikey;
    const bodyPayload = {
        postBody: `access_token=${access_token}&providerId=google.com`,
        requestUri: process_URL,
        returnIdpCredential: true,
        returnSecureToken: true
    };
    await axios.post(firebase_token_URL, bodyPayload)
        .then((res)=> {
            // 取得firebase服務的 idToken(與google其他服務分開)
            console.log('res.data', res.data)
            console.log(res.data.rawUserInfo)
            console.log(res.data.fullName)
            firebase_id_token = res.data.idToken;
            refresh_token = res.data.refreshToken;
            userInfos = res.data.rawUserInfo;
        })
        .catch((err)=>{
            console.log(err)
        });
    
    //將資料塞進前端
    res.redirect(`${referer}id_token=${firebase_id_token}&refresh_token=${refresh_token}&userInfos=${userInfos}`)

});

module.exports = app;