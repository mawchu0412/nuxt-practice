
// import * as storeTypes from '@/utility/constants.js'
import Cookies from 'js-cookie';
import jwtDecode from 'jwt-decode'


export const state = () => ({
  isUserLoggedIn: false, //是否登入
  // isOpenModal: false,
  userPicture: "", //會員照片
  userName: "", //會員名稱
  userUid: "", //會員 firebase 的 uid
  test_data:{
    aaa: 1,
    bbb: "string",
  },
  ajaxData:'',
  courses: [],
  coursesAmount: 0
})

export const getters = {
  get_isUserLoggedIn: state => {
    // console.log('getters this 有冒號', this)
    return state.isUserLoggedIn;
  },
  get_userPicture: state => {
    return state.userPicture;
  },
  get_userName: state => {
    return state.userName;
  },
  get_userUid: state => {
    return state.userUid;
  },
  // get_isOpenModal: state => {
  //   // console.log('getters this 有冒號', this)
  //   return state.isOpenModal;
  // },
  get_aaa: state => {
    // console.log('getters this 有冒號', this)
    return state.test_data.aaa;
  },
  get_aaabbb: state => {
    return state.test_data.aaa + state.test_data.bbb;
  },
  hello (state) {
    // console.log('getters this 沒有冒號', this)
  },
  get_courses: state => {
    return state.courses;
  },
  get_coursesAmount: state => {
    return state.coursesAmount;
  }
}

export const mutations = {
  /* 移植以下 cookie 塞資料的程式到SSR鉤子 ServerInit */
  set_user_OAuth: (state, payload) => {
    // console.log('payload', payload)
    // console.log(JSON.parse(payload.isUserLoggedIn))
    state.isUserLoggedIn = JSON.parse(payload.isUserLoggedIn);
    state.userPicture = payload.userPicture || 'https://bulma.io/images/placeholders/128x128.png';
    state.userName = unescape(payload.userName);
    state.userUid = payload.userUid;

    // // 改成通過 Cookie 存取資料 寫在Store裡
    // // console.log(Cookies.get('id_token'))
    // if(Cookies.get('id_token')) {
    //   //登入過 讀取cookie
    //   state.isUserLoggedIn = Cookies.get('isUserLoggedIn');
    //   state.userPicture = Cookies.get('userPicture');
    //   state.userName = Cookies.get('userName');
    //   state.userUid = Cookies.get('userUid');
    // } else {
    //   //第一次登入 寫入cookie
    //   Cookies.set('id_token', payload.id_token);
    //   Cookies.set('isUserLoggedIn', true);
    //   Cookies.set('userPicture', payload.userInfos.picture || 'https://bulma.io/images/placeholders/128x128.png');
    //   Cookies.set('userName', payload.userInfos.name);
    //   Cookies.set('userUid', payload.id_token_decode.user_id);
    //   state.isUserLoggedIn = Cookies.get('isUserLoggedIn');
    //   state.userPicture = Cookies.get('userPicture');
    //   state.userName = Cookies.get('userName');
    //   state.userUid = Cookies.get('userUid');
    // }
  },
  set_user_OAuth_logout: (state, payload) => {
    //資料清空後移除Cookie
    state.isUserLoggedIn = false;
    state.userPicture = '';
    state.userName = '';
    state.userUid = '';
    Cookies.remove('id_token');
    Cookies.remove('isUserLoggedIn');
    Cookies.remove('userPicture');
    Cookies.remove('userName');
    Cookies.remove('userUid');
    // 回到首頁 store 透過$Nuxt 取得路由資料並且導回首頁
    $nuxt.$router.push({ name: 'index' });
    // console.log($nuxt)
  },
  add_test_data: (state, payload) => {
    console.log('mutations 有冒號', this)
    // state.test_data.title = payload.title
    state.test_data.aaa ++;
  },
  hello(state, payload) {
    console.log('mutations 沒有冒號', this)
  },
  changeState: (state, payload) => {
    state.ajaxData = payload;
  },
  // set_isOpenModal: (state, payload) => {
  //   state.isOpenModal = payload;
  // },
  set_courses: (state, payload) => {
    state.courses = payload.courses;
  },
  set_coursesAmount: (state, payload) => {
    state.coursesAmount = payload;
  },
}

export const actions = {
  /* serverInit 鉤子只能放 index.js 的 actions */
  nuxtServerInit ( { commit }, context ) {
    // console.log(context.query.refresh_token)
    if( context.query.id_token && context.query.refresh_token ) {
      const id_token = context.query.id_token;
      const id_token_decode =jwtDecode(context.query.id_token);
      const refresh_token = context.query.refresh_token;
      const userInfos = JSON.parse(decodeURIComponent(context.query.userInfos));
      // id_token, id_token_decode, refresh_token, userInfos

      //傳遞payload的格式要與重整之後相同
      commit('set_user_OAuth', {
        isUserLoggedIn: true,
        userPicture: userInfos.picture,
        userName: userInfos.name,
        userUid: id_token_decode.user_id
      })

      // 在後端設定 cookie
      context.res.setHeader('Set-Cookie', [
        `id_token=${id_token}`,
        `isUserLoggedIn=${true}`,
        `userPicture=${userInfos.picture}`,
        `userName=${escape(userInfos.name)}`,
        `userUid=${id_token_decode.user_id}`
      ])
    }
    //重整之後抓不到參數用
    // console.log(context.req.headers.cookie)
    // 如果有 cookie
    if(!!context.req.headers.cookie) {
      // console.log(context.req.headers.cookie)
      if(context.req.headers.cookie.indexOf('id_token')>-1){
        let cookieArr = context.req.headers.cookie.split(';');
        let cookieKeys = ['id_token', 'isUserLoggedIn', 'userPicture', 'userName', 'userUid'];
        let cookiePayload = {};
        _.forEach(cookieKeys, (key)=> {
          _.forEach(cookieArr, (cookie)=> {
            if(cookie.indexOf(key) >-1) {
              // console.log(cookie.split(key+'=')[1])
              cookiePayload[key]=cookie.split(key+'=')[1]
            }
          })
        })
        console.log('cookiePayload', cookiePayload)
        commit('set_user_OAuth', cookiePayload)
      }
    }
  },

  // async ajaxTest (context, payload) {
  //   console.log(context.$axios)
  //   let data = await this.$axios('/api/test');
  //   console.log(data.data)
  //   context.commit('changeState', data.data.message )
  // },
  ajaxTest: async function({commit, dispatch}, payload) {
    // console.log('context', context)
    // console.log(context.$axios) // undefined
    // let data = await this.$axios('/api/test');
    // console.log('物件命名寫法', data.data)
    // console.log(data.data.message)
    // commit('changeState', data.data.message )
    console.log('_C', _C);
    dispatch(_C.REMOVE_DATA)
    console.log(_.forEach)
  },
  ajaxCourses: async function({commit}, payload) {
    await this.$axios.get('/api/courses')
      .then((res) => {
        commit('set_courses', {...res.data})
        commit('set_coursesAmount', res.data.courses.length)
      })
      .catch((err) => {
        console.log(err)
      });
  },
}
