const isRequired = () => { throw new Error('param is required'); };

export function rules(ruleName = isRequired(), value = isRequired()) {
    const require = /^[-'a-z\u4e00-\u9eff]{1,20}$/i;
    const englishNnumber = /^(?=.*?\d)(?=.*?[a-zA-Z])[a-zA-Z\d]+\S{5,}$/;
    const ruleNames = {
        name: require,
        password: englishNnumber,
        email: /.+@.+\..+/,
        phone: /[0][9][0-9]{8}/g
    };

    //new RegExp(/.../).test(value)
    const result = new RegExp(ruleNames[ruleName]).test(value);
    return result;
}